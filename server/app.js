'use strict'

const express = require('express');
const bodyparser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const port = process.env.PORT || 8081;

var app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

app.get('/', (req, res) => res.send('Welcome to My Pet Care Server'));
app.use('/vaccine', require('./src/vaccine/vaccineRoutes'));

app.use((req, res, next) => {
    var err = new Error('Not found');
    err.status = 404;
    next(err);
});

app.use((err, req, res) => {
    res.status(err.status || 500);
    res.end(JSON.stringify({
        message: err.message,
        error: {}
    }));
});

app.listen(port, (err) => {
    if (err){
        console.log('Error starting server.');
        process.exit(-1);
    }
    console.log('Server running on port ' + port);
})
