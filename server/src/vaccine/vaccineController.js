'use strict'

const Vaccine = require('../database/vaccine');

module.exports = {
    getVaccines: async (req, res) => {
        Vaccine.find().exec().then(data => {
            return res.status(200).json({data});
        }).catch(err => {
            return res.status(500).json(err);
        })
    },
    saveVaccine: async (req, res) => {
        var newV = new Vaccine({
            name: req.body.name,
            serial: req.body.serial,
            datetime: req.body.date,
            period: req.body.period,
            comments: req.body.comments
        });
        newV.save().then(() => {
            return res.status(200).json({message : 'Saved'});
        }).catch(err => {
            return res.status(500).json(err);
        });
    }
}