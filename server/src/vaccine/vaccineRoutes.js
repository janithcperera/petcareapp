'use strict'

const router = require('express-promise-router')();

const vaccineController = require('./vaccineController');

router.route('/')
    .get(vaccineController.getVaccines)
    .post(vaccineController.saveVaccine);

module.exports = router;