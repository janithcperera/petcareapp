const mongoose = require('./db');
const Schema = mongoose.Schema;

const vaccineSchema = new Schema({
    name: String,
    serial: String,
    datetime: {
        type: String,
        default: Date.now
    },
    period: String,
    comments: String
});

module.exports = mongoose.model('vaccine', vaccineSchema);