# MyPetCare App and Server

## App

#### Android Studio Version: 3.4

This application was developed to demonstrate read and write functionalities to a RESTful API

## Server

### Hosted location: https://mypetcareserver.herokuapp.com/ 

### Requests:

#### 1. GET: https://mypetcareserver.herokuapp.com/vaccine
##### Sample response: 
```
{
    "data": [
        {
            "_id": "id",
            "name": "name",
            "serial": "serial number",
            "datetime": "date",
            "period": "period",
            "comments": "comments",
            "__v": 0
        }
    ]
}
```


#### 2. POST: https://mypetcareserver.herokuapp.com/vaccine
##### Headers: Key-Content-Type:Value-application/json
##### Sample request body:
```
{
	"name": "Vaccine1",
    "serial": "VA000111222005",
    "date": "2022-12-22 00:00:00 PM",
    "period": "1 Year",
    "comments": "NA"
}
```

