package com.example.myapplication.Model;

public class Vaccine {

    private String date;
    private String period;
    private String comments;
    private String serial;
    private String name;

    public Vaccine(String name,String serial,String date, String period, String comments ) {
        this.name = name;
        this.serial = serial;
        this.date = date;
        this.period = period;
        this.comments = comments;


    }

    public Vaccine() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
