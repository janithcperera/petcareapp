package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.Adapter.VaccineHistoryAdaptor;
import com.example.myapplication.Model.Vaccine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewHistoryActivity extends AppCompatActivity {


    private List<Vaccine> vaccineList = new ArrayList<>();
    private RecyclerView recyclerView;
    private VaccineHistoryAdaptor mAdapter;

    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history);

        recyclerView = (RecyclerView) findViewById(R.id.result_list);
        mAdapter = new VaccineHistoryAdaptor(vaccineList);

        getSupportActionBar().setTitle("Vaccine History");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);


        recyclerView.setAdapter(mAdapter);

        mQueue = Volley.newRequestQueue(this);

        populateData();
    }

    private void populateData() {
        String url = "https://mypetcareserver.herokuapp.com/vaccine";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    UpdateList(response.getJSONArray("data"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);

    }

    private void UpdateList(JSONArray jsonArray){
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject vaccine = jsonArray.getJSONObject(i);

                vaccineList.add(new Vaccine(
                        vaccine.getString("name"),
                        vaccine.getString("serial"),
                        vaccine.getString("datetime"),
                        vaccine.getString("period"),
                        vaccine.getString("comments")));

            }
            mAdapter.notifyDataSetChanged();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
