package com.example.myapplication.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myapplication.Model.Vaccine;
import com.example.myapplication.R;

import java.util.List;

public class VaccineHistoryAdaptor extends RecyclerView.Adapter<VaccineHistoryAdaptor.VaccineViewHolder> {
    private List<Vaccine> vaccineList;

    @NonNull
    @Override
    public VaccineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.vaccinate_history_row, viewGroup, false);

        return new VaccineViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VaccineViewHolder holder, int position) {
        Vaccine vaccine = vaccineList.get(position);
        holder.name.setText(vaccine.getName());
        holder.serial.setText(vaccine.getSerial());
        holder.date.setText(vaccine.getDate());
        holder.period.setText(vaccine.getPeriod());
        holder.comment.setText(vaccine.getComments());
    }

    @Override
    public int getItemCount() {
        return vaccineList.size();
    }


    public class VaccineViewHolder extends RecyclerView.ViewHolder {
        public TextView name, serial, date, period, comment;


        public VaccineViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            serial = (TextView) view.findViewById(R.id.serial);
            date = (TextView) view.findViewById(R.id.date);
            period = (TextView) view.findViewById(R.id.date);
            comment = (TextView) view.findViewById(R.id.comment);

        }
    }


    public VaccineHistoryAdaptor(List<Vaccine> vaccineList) {
        this.vaccineList = vaccineList;
    }
}
