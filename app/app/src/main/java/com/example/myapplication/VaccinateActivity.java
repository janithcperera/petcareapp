package com.example.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Model.Vaccine;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class VaccinateActivity extends AppCompatActivity {

    Button btnSave;
    EditText name,serialNo, date, period, comment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccinate);

        btnSave =(Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAPI();

            }
        });

        name =(EditText) findViewById(R.id.input_name);
        serialNo =(EditText) findViewById(R.id.input_serial_no);
        date =(EditText) findViewById(R.id.input_date);
        period =(EditText) findViewById(R.id.input_period);
        comment =(EditText) findViewById(R.id.input_comment);


    }

   public void callAPI() {
       Thread thread = new Thread(new Runnable() {
           @Override
           public void run() {
               try {
                   URL url = new URL("https://mypetcareserver.herokuapp.com/vaccine");
                   HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                   conn.setRequestMethod("POST");
                   conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                   conn.setRequestProperty("Accept","application/json");
                   conn.setDoOutput(true);
                   conn.setDoInput(true);

                   JSONObject jsonParam = new JSONObject();
                   jsonParam.put("name", name.getText());
                   jsonParam.put("serial", serialNo.getText());
                   jsonParam.put("date", date.getText());
                   jsonParam.put("period", period.getText());
                   jsonParam.put("comments", comment.getText());

                   Log.i("JSON", jsonParam.toString());
                   DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                   //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                   os.writeBytes(jsonParam.toString());

                   os.flush();
                   os.close();

                   Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                   Log.i("MSG" , conn.getResponseMessage());

                   conn.disconnect();

                   Intent intent = new Intent(VaccinateActivity.this, MainActivity.class);
                   VaccinateActivity.this.startActivity(intent);
               } catch (Exception e) {
                   e.printStackTrace();
               }
           }
       });

       thread.start();
   }
}
